
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(800, 584)
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(270, 50, 261, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 100, 55, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 140, 241, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(30, 120, 251, 20))
        self.label_4.setObjectName("label_4")
        self.searchbutton = QtWidgets.QPushButton(self.centralwidget)
        self.searchbutton.setGeometry(QtCore.QRect(410, 120, 291, 41))
        self.searchbutton.setObjectName("searchbutton")
        self.result = QtWidgets.QLabel(self.centralwidget)
        self.result.setGeometry(QtCore.QRect(20, 240, 441, 21))
        self.result.setObjectName("result")
        self.endresult = QtWidgets.QLabel(self.centralwidget)
        self.endresult.setGeometry(QtCore.QRect(20, 280, 751, 211))
        self.endresult.setText("")
        self.endresult.setTextFormat(QtCore.Qt.AutoText)
        self.endresult.setObjectName("endresult")
        mainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(mainWindow)
        self.statusbar.setObjectName("statusbar")
        mainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

        self.add_functions()

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "Поиск совпадающих элементов"))
        self.label.setText(_translate("mainWindow", "Поиск совпадающих элементов в списке"))
        self.label_2.setText(_translate("mainWindow", "Списки:"))
        self.label_3.setText(_translate("mainWindow", "5, 3, \'Hello\', 7, 12.5, \'Python\', True, False"))
        self.label_4.setText(_translate("mainWindow", "2, False, 9.1, (2 - 1j), \'hello\', 5, \'Python\'"))
        self.searchbutton.setText(_translate("mainWindow", "Поиск"))
        self.result.setText(_translate("mainWindow", "Результат:"))

    def add_functions(self):
        self.searchbutton.clicked.connect(self.sortAB)

    def sortAB(self):
        # Первый список
        A = [2, False, 9.1, (2 - 1j), 'hello', 5, 'Python']
        # Второй список
        B = [5, 3, 'Hello', 7, 12.5, 'Python', True, False]
        i = 0
        for a in A:
            # Новый индекс элемента из 1-го списка
            i = i + 1
            # Индекс для нумерации элементов 2 -го списка
            j = 0
            # Внутренний оператор цикла .
            # Перебираем элементы 2-го списка
            for b in B:
                # Новый индекс элемента из _ l -го списка
                j = j + 1
                # Условный оператор . Проверяем
                # равенство элементов
                if a == b:
                    # Е сли элементы совпадают
                    txt = str(i) + "-й элемент из 1 - го списка и "
                    txt = txt + str(j) + "-й элемент из 2 -го списка "
                    print(txt)
                    self.endresult.setText(self.endresult.text() + f'<html><head/><body><p align="center"><b>{txt}</p></body></html>')

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = QtWidgets.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    mainWindow.show()
    sys.exit(app.exec_())
